# put-dns

Debian packaging for put-dns, a simple interface to create DNS records with various DNS providers

The put-dns source can be found at https://gitlab.com/JohnLines/put-dns,
with some documentation at https://gitlab.com/JohnLines/put-dns/-/wikis/Overview

Debian packaging can be found at https://salsa.debian.org/jlines/put-dns
