# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the put-dns package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: put-dns\n"
"Report-Msgid-Bugs-To: put-dns@packages.debian.org\n"
"POT-Creation-Date: 2021-02-22 08:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Automatically update domain in put-dns.conf?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"If enabled put-dns will, on configuration, try to detect a valid external "
"domain name on the current system and update the put-dns.conf configuration "
"file to use it. If disabled then the configuration will not be automatically "
"updated."
msgstr ""
